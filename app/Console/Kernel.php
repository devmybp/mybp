<?php

namespace App\Console;

use App\Jobs\controle_ponto\VerificaJornadasJob;
use App\Jobs\Weekly_report\LembreteTarefaJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')->hourly();
        //$schedule->command('inspire')->everyMinute();

        $schedule->call(new LembreteTarefaJob)->everyMinute();
        //$schedule->call(new VerificaJornadasJob)->everyMinute();
        $schedule->call(new VerificaJornadasJob)->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
