<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DocumentosPreAdmissao
 *
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentosPreAdmissao newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentosPreAdmissao newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DocumentosPreAdmissao query()
 * @mixin \Eloquent
 */
class DocumentosPreAdmissao extends Model
{
    //
}
