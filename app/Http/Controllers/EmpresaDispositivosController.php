<?php

namespace App\Http\Controllers;

use App\Models\EmpresaDispositivos;
use Illuminate\Http\Request;

class EmpresaDispositivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmpresaDispositivos  $empresaDispositivos
     * @return \Illuminate\Http\Response
     */
    public function show(EmpresaDispositivos $empresaDispositivos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmpresaDispositivos  $empresaDispositivos
     * @return \Illuminate\Http\Response
     */
    public function edit(EmpresaDispositivos $empresaDispositivos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmpresaDispositivos  $empresaDispositivos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmpresaDispositivos $empresaDispositivos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmpresaDispositivos  $empresaDispositivos
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmpresaDispositivos $empresaDispositivos)
    {
        //
    }
}
