<?php

namespace App\Http\Controllers;

use App\Models\AfastamentoFeedback;
use DB;
use Illuminate\Http\Request;
use MasterTag\DataHora;
use PDF;

class AfastamentoFeedbackController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->input();

        $dadosValidados = \Validator::make($dados, [
            'data_inicio' => 'required',
            'data_fim' => 'required'
        ]);

        if ($dadosValidados->fails()) { // se o array de erros contem 1 ou mais erros..
            return response()->json([
                'msg' => 'Erro ao Salvar Afastamento',
                'erros' => $dadosValidados->errors()
            ], 400);
        } else {
            try {
                DB::beginTransaction();

                $dados['quem_cadastrou'] = auth()->id();

                AfastamentoFeedback::create($dados);

                DB::commit();
                return response()->json([], 201);
            } catch (\Exception $e) {
                DB::rollback();
                $msg = "error STORE AFASTAMENTO FEEDBACK:  {$e->getMessage()} , {$e->getCode()}, {$e->getLine()} | Usuario: " . auth()->user()->nome;
                \Log::debug($msg);
//                return response()->json(['msg' => 'Houve um erro por favor tente novamente!'], 400);
                return response()->json(['msg' => $msg], 400);
            }
        }
    }

    public function afastamentoPDF($id, $feedback_id)
    {
        $afastamento = AfastamentoFeedback::with('Usuario', 'Feedback')->whereId($id)->whereFeedbackId($feedback_id)->first();
        $pdf = PDF::loadView('pdf.admissao.historico.afastamento.ficha', compact('afastamento'));
        $pdf->setPaper('A4', 'portrait');
        return $pdf->stream((new DataHora())->nomeUnico() . ".pdf");

    }

}
