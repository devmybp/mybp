@extends('layouts.sistema')
@section('content_header','Início')
@section('content')
    <modal id="termosdeuso" titulo="TERMO DE USO E RESPONSABILIDADE" size="g">
        <template slot="conteudo">
            <div class="col-12 termos">
                <h4 class="text-center"><strong>TERMO DE USO E RESPONSABILIDADE</strong></h4>
                <br>
                <p class="text-justify">
                    <strong>BUSINESS PARTNERS SERVIÇOS EMPRESASRIAIS EIRELI,</strong> com sede na Rua Avenida dos
                    Holandeses, n.17,
                    bairro olho d’água, Centro Comercial Luminy Plaza, loja 03, São Luis/MA, CEP: 65.065-180, inscrita
                    no CNPJ 22.055.835/0001-81, doravante denominada BPSE, e o Contratante devidamente qualificado no
                    termo de aceite, doravante denominado CLIENTE, partes que compõem o presente instrumento acordam
                    que:
                </p>
                <p class="text-justify">
                    <strong>CLÁUSULA PRIMEIRA - DO OBJETO E DEFINIÇÕES</strong>
                    <br>
                    1. O presente Termo tem como objeto a Cessão de Uso do SISTEMA DE GESTÃO INTERNO - SGI de
                    propriedade exclusiva da BPSE, disponível para o CLIENTE para acesso online, por meio de acesso
                    remoto ao servidor da BPSE, conforme as diretrizes e condições a seguir.
                    <br>
                    <br>
                    2. Adaptações corretivas da plataforma, assistência técnica para utilização do software e Suporte
                    Metodológico Online:
                    <br>
                    <br>
                    2.1 O suporte Metodológico será prestado somente ao cliente que esteja com o Termo de Uso e
                    Responsabilidade presente vigente, ou seja, clientes do modelo de mensalidades ou anuidades com
                    pagamento em dia.
                    <br>
                    <br>
                    3. Sob os termos e condições adiante, a BPSE prestará ao CLIENTE os serviços disponíveis por meio do
                    uso da plataforma, conforme especificações previstas na Proposta Comercial aceita pelo CLIENTE e que
                    faz parte deste Termo de Uso e Responsabilidade.
                    <br>
                    <br>
                    4. O SISTEMA DE GESTÃO INTERNO – SGI BPSE é uma plataforma online que funciona em nuvem, na qual
                    permite a BPSE conduzir de forma online, enxuta, eficiente, segura e com rastreabilidade todos os
                    fluxo de trabalho referente ao gerenciamento dos subprocessos de recursos humanos e gestão da
                    qualidade.
                </p>
                <p class="text-justify">
                    <strong>CLÁUSULA SEGUNDA - DAS OBRIGAÇÕES DA BPSE</strong>
                    <br>
                    1. Manter os serviços contratados com um SLA - Service Level Agreement de 95% (noventa por cento),
                    até o encerramento deste Termo de Uso e Responsabilidade. Entende-se por Service Level Agreement de
                    95% como sendo que a plataforma deve estar ativa e acessível por pelo menos 95% (noventa e cinco) do
                    tempo total de horas em cada mês. Caso a plataforma apresente disponibilidade por tempo inferior a
                    95%, a BPSE irá ressarcir em iguais períodos sem custo adicional para o CLIENTE. Desde que seja
                    comprovada a indisponibilidade do mesmo
                    <br>
                    <br>
                    2. Correção de problemas eventuais, detectados pelo CLIENTE e identificados como sendo do software
                    com prazo máximo de 10 (dez) dias úteis contados da data em que forem reportados pelo
                    suporte@bpse.com.br
                    <br>
                    Prazo para atendimento: <br>
                    Indisponibilidade da plataforma: 24h <br>
                    Indisponibilidade de um menu da plataforma: 4 dias <br>
                    Problema Contornável: 7 dias <br>
                    Problemas de baixo impacto: 10 dias
                    <br>
                    <br>
                    3. A BPSE se obriga a cientificar os seus prepostos e empregados do caráter sigiloso das informações
                    e documentos fornecidos pelo CLIENTE à BPSE por força do presente pacto, tomando as medidas devidas
                    para que estas informações e dados somente sejam divulgadas àqueles funcionários que delas dependam
                    para a execução dos serviços, nunca a terceiros.
                    <br>
                    <br>
                    4. Oferecer suporte online gratuito em relação ao uso da plataforma, o qual será acionado através do
                    e-mail suporte@bpse.com.br ou via formulário disponível na plataforma. O retorno será realizado em
                    dia útil e durante o horário comercial (das 9h00 às 17h00 – hora de Brasília, BRT).
                </p>

                <p class="text-justify">
                    <strong>CLÁUSULA TERCEIRA - DAS OBRIGAÇÕES DO CLIENTE</strong>
                    <br>
                    1. O CLIENTE se compromete a restringir o uso da plataforma, desenvolvido pela BPSE, exclusivamente
                    a
                    empresa contratante, bem como se responsabilizará pela guarda das informações de acesso a
                    plataforma, como usuário e senha, fornecidos pela BPSE.
                    <br>
                    <br>
                    2. O acesso a plataforma somente será permitido para sócios, diretores, gerentes e funcionários
                    ligados diretamente ao CLIENTE, configurando-se violação de direitos autorais a cessão ou
                    facilitação do acesso de terceiros.
                    <br>
                    <br>
                    3. É extremamente vedada a modificação, cópia parcial ou total de qualquer texto, logomarca ou
                    gráfico, distribuição, transmissão, exibição, execução, reprodução, publicação, criação de trabalho
                    derivado de transferência, venda ou qualquer outro tipo de disponibilização dos produtos de algum
                    menu do SGI BPSE, de créditos de acesso a plataforma, ou de informações e pesquisas nela geradas, a
                    terceiros, configurando- se tais ações em crimes contra os direitos autorais e civis, sujeitando o
                    CLIENTE a responder por perdas e danos verificados, crime de concorrência desleal, bem como violação
                    de direito autoral previsto na Lei 9.609/98.
                    <br>
                    <br>
                    4. O CLIENTE se obriga a cientificar os seus prepostos e empregados do caráter sigiloso das
                    informações, documentos, arquivos, programas e documentação de programas, aos quais tiverem acesso,
                    tomando as medidas devidas para que estas informações somente sejam divulgadas àqueles funcionários
                    que delas dependam para a execução dos serviços, nunca a terceiros.
                    <br>
                    <br>
                    5. O CLIENTE se compromete a efetuar, em dia, o pagamento pelos serviços, sendo o não cumprimento
                    causa de suspensão ou bloqueio da plataforma, bem como atendimentos de suporte.
                    <br>
                    <br>
                    6. Ao CLIENTE, não é permitido:
                    <br>
                    <br>
                    6.1 utilizar a plataforma para qualquer finalidade ilegal ou proibida por estes termos, condições,
                    avisos ou em desacordo com a legislação brasileira e os Tratados e Convenções Internacionais o
                    Brasil é signatário.
                    <br>
                    <br>
                    6.2 usar a plataforma de qualquer maneira que possa danificar, desativar, sobrecarregar ou
                    prejudicar o Servidor ou interferir no seu uso ou aplicação por terceiros.
                    <br>
                    <br>
                    6.3 tentar obter acesso não autorizado a qualquer conta, rede ou software de computador associado à
                    BPSE.
                    <br>
                    <br>
                    6.4 obter ou tentar obter qualquer material ou informação, por qualquer meio não disponibilizado ou
                    não fornecido, intencionalmente, através da BPSE.
                </p>

                <p class="text-justify">
                    <strong>CLÁUSULA QUARTA - DOS DIREITOS</strong>
                    <br>
                    1.A plataforma e outras funcionalidades, de que trata este Termo de Uso e Responsabilidade, são de
                    propriedade da BPSE, que cede ao CLIENTE o direito de seu uso pelo prazo de duração deste Termo de
                    Uso e Responsabilidade.
                    <br>
                    <br>
                    2.Os dados inseridos na plataforma pelo CLIENTE, são de propriedade do CLIENTE. Os dados gerados
                    pela plataforma são de propriedade da BPSE de uso compartilhado com o CLIENTE. Os dados processados
                    pela BPSE e a inteligência de dados gerada pelo plataforma são de propriedade da BPSE cedidas ao
                    CLIENTE.
                    <br>
                    <br>
                    3. A BPSE não se responsabiliza por decisões tomadas pelo CLIENTE com base nas análises dos
                    Resultado Integrado ou ainda com informações geradas pela plataforma.
                    <br>
                    <br>
                    4.As manutenções adaptativas e evolutivas sobre os módulos da plataforma são de propriedade
                    exclusiva da BPSE. Os programas gerados em cada nova versão da plataforma somente poderão ser
                    utilizados pelo CLIENTE em sua versão executável.
                    <br>
                    <br>
                    5.É expressamente proibido falsificar ou excluir qualquer atribuição de autoria, avisos legais ou
                    outros avisos apropriados, designações, rótulos de propriedade da origem, fonte da plataforma ou
                    material contido em um arquivo transferido, configurando-se tais ações em crimes contra os direitos
                    autorais e civis.
                    <br>
                    <br>
                    6. A BPSE reserva-se o direito de determinar e implementar aprimoramentos na plataforma, de acordo
                    com a sua conveniência e com as necessidades de outros de seus clientes, desde que não o
                    descaracterize, nem conflite com os processos informatizados já em prática pelo CLIENTE.
                    <br>
                    <br>
                    7. A BPSE pode citar o cliente e sua marca na mídia como um de seus clientes. Exceto se o cliente se
                    manifestar de forma contrária.
                </p>

                <p class="text-justify">
                    <strong>CLÁUSULA QUINTA - DOS PREÇOS E REAJUSTAMENTO</strong>
                    <br>
                    1.Para a execução dos serviços descritos neste Termo de Uso e Responsabilidade, o CLIENTE remunerará
                    a BPSE da forma como descrita no Termo de Aceite e na Proposta Comercial, considerando-os anexos a
                    esse Termo de Uso e Responsabilidade. O pagamento do valor será realizado por meio de cartão de
                    créditos, transferência ou boleto bancário emitido pela BPSE.
                    <br>
                    <br>
                    2. Os preços estabelecidos neste Termo de Uso e Responsabilidade serão reajustados anualmente,
                    conforme determina a legislação atual, ou na menor periodicidade permitida pela legislação vigente
                    no país, caso isto venha a ser permitido, tendo como índice oficial indexador o IGP-M, divulgado
                    pela Fundação Getúlio Vargas ou, na falta deste, pelo índice que venha a substituí- lo.
                    3. Caso o CLIENTE solicite acessos além do que consta no plano adquirido, será cobrado um adicional
                    no valor de cada acesso disponibilizado.
                    <br>
                    <br>
                    4. Os impostos hoje incidentes sobre esta prestação de serviços já estão inclusos no valor do
                    faturamento. Quaisquer novos impostos que venham a incidir sobre os serviços serão objeto de
                    negociação entre as partes.
                    <br>
                    <br>
                    5. Os pagamentos realizados em atraso serão atualizados monetariamente até a data do pagamento pela
                    variação percentual do IGP-M, ocorrida entre a data do vencimento e a liquidação, e terão o
                    acréscimo de juros de 1% (um por cento) ao mês, estes calculados “pro rata dia”, bem como multa de
                    2% (dois por cento) pelo atraso no pagamento.
                    <br>
                    <br>
                    6. No caso de atrasos de pagamento superiores a 5 dias corridos, será suspenso o acesso do CLIENTE a
                    plataforma, mediante avisos enviados pela BPSE, e na ausência pagamento superior a 30 (trinta) dias
                    poderá importar no cancelamento do contrato e bloqueio da plataforma, estando assim sujeito cobrança
                    judicial e extrajudicial

                </p>


                <p class="text-justify">
                    <strong>CLÁUSULA SEXTA - DO PRAZO, PRORROGAÇÃO E RESCISÃO</strong>
                    <br>
                    1.O presente Termo de Uso e Responsabilidade terá o prazo mínimo de 12 (doze) meses, conforme Termo
                    de Aceite online preenchido pelo CLIENTE, o qual será renovado automaticamente por iguais períodos,
                    cujo marco inicial será o preenchimento do referido termo de aceite.
                    <br>
                    <br>
                    2. Após o prazo mínimo do Termo de Uso e Responsabilidade de 12 (doze) meses, qualquer das partes
                    poderá rescindi-lo, mediante comunicado realizado com 30 (trinta) dias de antecedência, via email.
                    <br>
                    <br>
                    3. Caso o pedido de cancelamento seja enviado antes do prazo mínimo do Termo de Uso e
                    Responsabilidade de 12 (doze) meses, o CLIENTE deve arcar com:
                    <br>
                    <br>
                    3.1 O pagamento de multa correspondente à 20% do valor referente às mensalidades vincendas até
                    completar o 12º. (décimo segundo) mês.
                    <br>
                    <br>
                    4. Após a solicitação formal de cancelamento, o CLIENTE terá até 30 (trinta) dias para utilização
                    antes da desativação da conta.
                    <br>
                    <br>
                    5. Em caso de não cumprimento por uma das partes dos compromissos estabelecidos no presente
                    instrumento, a outra parte poderá considerar rescindido, de pleno direito, este Termo de Uso e
                    Responsabilidade a qualquer momento e sem multa rescisória, enviando comunicado por escrito e
                    detalhado do ocorrido, protocolado ou pelos Correios, via carta com Aviso de Recebimento.
                </p>

                <p class="text-justify">
                    <strong>CLÁUSULA SÉTIMA - CONFIDENCIALIDADE E PROTEÇÃO DE DADOS</strong>
                    <br>
                    1.Cada parte se compromete a manter e tratar com sigilo e não revelar a terceiros qualquer
                    informação confidencial relacionada ao sistema, serviços, dados de usuários, segredo de negócios,
                    estratégias e outros, ou usar o privilégio do acesso as referidas informações para propósito
                    conflitante que não seja o previsto no presente Termo.
                    <br>
                    <br>
                    2. As Partes se comprometem a adotar as melhores práticas para respeitar a legislação vigente e/ou
                    que venha a entrar em vigor sobre proteção de dados, sendo certo que se adapta, inclusive à Lei nº
                    13.709/2018.
                    <br>
                    <br>
                    3. Este instrumento está sujeito ao cumprimento da legislação vigente de proteção de dados, quanto à
                    coleta, tratamento, processamento e armazenamento dos dados pessoais e sensíveis. Cumpre mencionar
                    que a coleta dos referidos dados são imprescindíveis para a operacionalização da plataforma conforme
                    base legal aplicada para os fins do serviços prestado pela nossa tecnologia.
                    <br>
                    <br>
                    4. Cada Parte se compromete a cientificar e instruir seus colaboradores quanto proteção e
                    privacidade de dados pessoais e sensíveis, bem como sigilo e confidencialidade de quaisquer
                    informações oriundas deste instrumento.
                    <br>
                    <br>
                    5. O Cliente declara que possui o consentimento de todos os seus colaboradores, para que seus dados
                    possam ser utilizados para os fins deste contrato.
                    <br>
                    <br>
                    6. Os Titulares dos dados pessoais, detém o direito de exigir a qualquer momento que seus dados
                    pessoais sejam retificados, atualizados e/ou excluídos.
                    <br>
                    <br>
                    7. A BPSE possui o direito de utilizar os dados de forma anônima para fins estatísticos e de
                    pesquisa para melhorar as atualizações das ferramentas oferecidas respeitando as especificações da
                    Lei, quanto a anonimização e impossibilidade de identificação do indivíduo.
                    <br>
                    <br>
                    8. Ficam as partes cientes que os dados podem ser modificados conforme as necessidades dos termos.
                    (permitir que os dados sejam utilizados pela bpse para envio de emails comerciais)

                </p>

                <p class="text-justify">
                    <strong>CLÁUSULA OITAVA - DAS CONDIÇÕES ESPECIAIS E DEFINIÇÕES COMPLEMENTARES</strong>
                    <br>
                    1.A eventual omissão ou liberalidade de qualquer das partes em exigir o cumprimento dos termos e
                    condições deste Termo de Uso e Responsabilidade ou em exercer uma prerrogativa dele decorrente não
                    constituirá renúncia, nem afetará o direito da parte de exercê-lo a qualquer tempo.
                    <br>
                    <br>
                    2. Este Termo de Uso e Responsabilidade é considerado aceito pelo CLIENTE e passa a vigorar após o
                    preenchimento do Termo de Aceite e confirmado pelo primeiro pagamento efetuado pelo CLIENTE à BPSE.
                    <br>
                    <br>
                    3. A BPSE não garante perenidade das integrações com plataformas de terceiros. Estas podem degradas
                    ou deixar de existir por conta e alterações de políticas da empresa terceira.
                    <br>
                    <br>
                    4. O Termo de Uso e Responsabilidade é de natureza estritamente civil, inexistindo qualquer vínculo
                    empregatício entre a CONTRATANTE e os empregados, dirigentes ou prepostos da CONTRATADA e vice e
                    versa, correndo por conta exclusiva de quem der causa, todas as despesas com o respectivo pessoal,
                    incluindo os respectivos salários, encargos trabalhistas, previdenciários e quaisquer outras
                    parcelas de qualquer natureza porventura relacionadas ao referido vínculo.

                </p>

                <p class="text-justify">
                    <strong>CLÁUSULA NONA - DO FORO</strong>
                    <br>
                    9.1. As Partes elegem o Foro da Comarca de São Luis-MA para a solução de quaisquer litígios
                    decorrentes deste Termo de Uso e Responsabilidade.
                </p>

            </div>
        </template>
    </modal>
    <modal id="termosdeuso_popup" titulo="TERMO DE USO E RESPONSABILIDADE" :fechar="false">
        <template slot="conteudo">
            <div class="row">
                <div class="col-12">
                    <p class="text-justify">Os Termos de Uso e Política de Privacidade do SGIBPSE foram atualizados em
                        30/11/2020.
                        <br>Para continuar, é necessário ler e aceitar o documento atualizado.</p>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="check" v-model="check">
                        <label class="form-check-label" for="check">Declaro que li e aceito os
                            <a href="javascript://" data-toggle="modal" data-target="#termosdeuso">termos</a></label>
                    </div>
                </div>
            </div>
        </template>
        <template slot="rodape">
            <button class="btn btn-primary btn-block" :disabled="!check" @click="concordar" style="font-size: 0.9rem">
                CONFIRMAR
            </button>
        </template>
    </modal>

    <div class="row">

        <div id="sandbox-container"></div>

        <div class="col-12 col-lg-10">
            <div class="row">
                <div class="col-xl-5 col-lg-6 mt-3">
                    <div class="card card-stats mb-4 mb-xl-0 text-white"
                         style="background: url(https://site.bpse.com.br/img/b_blue.png) no-repeat #072333; background-size: cover; min-height: 350px">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-white mt-5 mb-4">Missão</h5>
                                    <p class="">Nós queremos ser os MELHORES. Por isso, desenvolvemos e
                                        executamos estratégias para
                                        atuar como protagonista de transformação no negócio, gerando uma troca benéfica,
                                        encantadora, rentável, duradoura e exclusiva entre as partes envolvidas.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-5 col-lg-6 mt-3">
                    <div class="card card-stats mb-4 mb-xl-0 text-white"
                         style="background: url(https://site.bpse.com.br/img/b_blue.png) no-repeat #072333; background-size: cover; min-height: 350px">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-white mt-5 mb-4">Visão</h5>
                                    <p class="">Fazer da BPSE a mais EFICIENTE, ATRATIVA e COBIÇADA
                                        marca de
                                        serviços e soluções em Gestão de RH e QSSMA no Maranhão até 2022.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-5 col-lg-6 mt-3">
                    <div class="card card-stats mb-4 mb-xl-0 text-white"
                         style="background: url(https://site.bpse.com.br/img/b_blue.png) no-repeat #072333; background-size: cover; min-height: 350px">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-white mt-5 mb-4">Valores</h5>
                                    <p class="">
                                        <i class="fa fa-check"></i> Respeito é o princípio <br>
                                        <i class="fa fa-check"></i> Engajamento e entusiasmo <br>
                                        <i class="fa fa-check"></i> Agilidade com qualidade <br>
                                        <i class="fa fa-check"></i> Sentimento de dono <br>
                                        <i class="fa fa-check"></i> Amor em cada detalhe <br>
                                        <i class="fa fa-check"></i> Foco em resultado sustentável <br>
                                        <i class="fa fa-check"></i> Ética e transparência <br>
                                        <i class="fa fa-check"></i> Desenvolvimento de pessoas
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-5 col-lg-6 mt-3">
                    <div class="card card-stats mb-4 mb-xl-0 text-white"
                         style="background: url(https://site.bpse.com.br/img/b_blue.png) no-repeat #072333; background-size: cover; min-height: 350px">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-white mt-5 mb-4">POLÍTICA DE GESTÃO DA
                                        QUALIDADE</h5>
                                    <p class="">
                                        A BPSE é uma empresa que oferece Soluções em Gestão de Recursos Humanos,
                                        Departamento de
                                        Pessoal, Saúde e Segurança & Meio Ambiente, Treinamentos e Desenvolvimento, nos
                                        estados
                                        do MA, PA e SP. <br>

                                        Com o Jeito BPSE de atender, buscamos parcerias duradouras agregando valor aos
                                        negócios
                                        dos clientes, garantindo o atendimento dos requisitos legais e estatutários,
                                        trabalhando
                                        continuamente para o aperfeiçoamento dos serviços.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-2 d-flex">
            <img src="{{ asset('images/bep.png') }}" class="img-fluid align-self-end" alt="">
        </div>
    </div>
@endsection
@push('js')

    <script type="text/javascript">
        const app = new Vue({
            el: '#app',
            data: {
                check: false
            },
            mounted: function() {
                @if(!auth()->user()->termos)
                $('#termosdeuso_popup').modal('show')
                @endif
            },
            methods: {
                concordar() {
                    axios.put(`${URL_ADMIN}/concordarTermos`).then(response => {
                        if (response.status === 201) {
                            $('#termosdeuso_popup').modal('hide')
                            // mostraSucesso('', '')
                        }
                    })
                }
            }
        })

    </script>
@endpush
